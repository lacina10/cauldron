'use strict';

class CommentCounter {
    voteContainer;

    constructor() {
        this.voteContainer = document.querySelectorAll('div.vote-arrows');
        this.voteContainer.forEach(el => el.addEventListener('click', this.voteCounter));
        console.log('comment vote count is started')
    }

    voteCounter = (event) => {
        const target = event.target.closest('a');
        const direction = target.dataset.direction;
        const voteView = target.parentElement.querySelector('span.total-vote');
        const promise = this.getCommentsVote(10, direction); //because we wan the this pointed on the object we can use arrow function or a bind method
        promise.then(data => {
            this.updateVoteCounterView(+data?.votes, voteView);
        })
    }

    async getCommentsVote(id, direction) {
        const response = await fetch(`/comments/${id}/vote/${direction}`);
        return await response.json();
    }

    updateVoteCounterView(votes, view) {
        if(!votes) return false;
        view.innerHTML = `${0 > votes ? '-': '+'} ${votes}`;
    }
}

//start the counter handling
export default CommentCounter;
