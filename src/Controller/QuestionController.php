<?php

namespace App\Controller;

use App\Service\GeneratorTestor;
use App\Service\MarkdownHelper;
use Knp\Bundle\MarkdownBundle\MarkdownParserInterface;
use Psr\Log\LoggerInterface;
use Sentry\State\HubInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\CacheInterface;

class QuestionController extends AbstractController {

  /**
   * @var bool
   */
  private $isDebug;

  private $logger;

  public function __construct(bool $isDebug, LoggerInterface $logger) {
    $this->isDebug = $isDebug;
    $this->logger = $logger;
  }


  /**
   * @Route("/", name="app_homepage")
   * @return Response
   */
  public function homepage(GeneratorTestor $generatorTestor) {
//    dump($generatorTestor->rangeGenerator());
//    foreach ($generatorTestor->randomNumbers(1) as $randomNumber){
//      dump($randomNumber);
//    }
//    $generator = $generatorTestor->rangeGenerator();
//    foreach ($generator as $range){
//      dump(sprintf('range: %s', $range));
//    }
    $sender = $generatorTestor->sendDataTestor();
    foreach ($sender as $number){
      if(5 === $number){
        $sender->send(10);
        dump($sender->current());
      }
      if(15 === $number){
        $sender->send(true);
        dump($sender->current());
      }
      dump($number);
    }
    return $this->render('question/homepage.html.twig');
  }

  /**
   * @Route("/question/{slug}", name="app_question_show")
   * @return Response
   * @throws \Psr\Cache\InvalidArgumentException
   */
  public function show(bool $isDebug, string $slug, MarkdownHelper $markdownHelper) { //, HubInterface $hub
//    dump($hub);
//    dump($isDebug);
//    dump($this->logger);
//    dump($this->getParameter('cache_adapter')); // get an parameters values

//    throw new \Exception('Bad stuff happened! in secret');
    $answers = [
      'Make sure your cat is sitting `purrrfectly` still ?',
      'Honestly, I like furry shoes better than MY cat',
      'Maybe... try saying the spell backwards?',
    ];
   $questionText = "**I've** been turned into a cat, any *thoughts* on how to turn back? While I'm **adorable**, I don't really like and care for cat food.";
    /*$questionTextMarkdown = $cache->get('markdown_'. $questionText, function() use ($questionText, $markdownParser){
        return $markdownParser->transformMarkdown($questionText);
    });*/
    $questionTextMarkdown = $markdownHelper->parse($questionText);
    //dump($slug, $this);
    return $this->render(
      'question/show.html.twig',
      [
        'question' => str_replace('-', ' ', $slug),
        'answers' => $answers,
        'questionText' => $questionTextMarkdown,
      ]
    );
    #return new Response(sprintf('Future page to show the question: "%s" !', str_replace('-', ' ', $slug)));
  }

}