<?php

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class CommentController extends AbstractController {

  /**
   * @Route("/comments/{id}/vote/{direction<up|down>}", name=
   *   "app_comment_vote", methods="POST|GET")
   *
   * @throws \Exception
   */
  public function commentVote(
    int $id,
    string $direction,
    LoggerInterface $logger
  ): JsonResponse {
    // todo use id to query database

    //use real logic here to save this to the DB
    $bornes = [];
    if ('up' === $direction) {
      $bornes = [50,100];
      $logger->info('voting up');
    }
    else {
      $bornes = [0, 49];
      $logger->info('voting down');
    }
    $currentVoteCount = random_int(...$bornes);

    return new JsonResponse(['votes' => $currentVoteCount]);
  }

}