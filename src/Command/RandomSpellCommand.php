<?php

namespace App\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class RandomSpellCommand extends Command {

  private const SPELLS = [
    'alohomora',
    'confundo',
    'engorgio',
    'expecto patronum',
    'expelliarmus',
    'impedimenta',
    'reparo',
  ];

  protected static $defaultName = 'app:random-spell';

  protected static $defaultDescription = 'Cast a random spell';

  private $logger;

  public function __construct(LoggerInterface $logger) {
    parent::__construct();
    $this->logger = $logger;
  }


  protected function configure(): void {
    $this
      ->addArgument('your-name', InputArgument::OPTIONAL, 'Your name')
      ->addOption('yell', NULL, InputOption::VALUE_NONE, 'Yell?');
  }

  protected function execute(
    InputInterface $input,
    OutputInterface $output
  ): int {
    $io = new SymfonyStyle($input, $output);
    $yourName = $input->getArgument('your-name');

    if ($yourName) {
      $io->note(sprintf('Hi: %s', $yourName));
    }

    //spelling
    $spell = $this->getSpell();

    if ($input->getOption('yell')) {
      $spell = strtoupper($spell);
    }

    $io->success($spell);

    $this->logger->info(sprintf('Casting spell %s', $spell));

    return Command::SUCCESS;
  }

  private function getSpell() {
    return self::SPELLS[array_rand(self::SPELLS)];
  }

}
