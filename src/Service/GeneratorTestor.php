<?php

namespace App\Service;

class GeneratorTestor {

  public function randomNumbers(int $number) {
    for ($i = 0; $i < $number; $i++) {
      yield mt_rand(0, $number);
    }
  }

  public function rangeGenerator() {
    for ($i = 1; $i < 3; $i++) {
      yield $i;
    }
  }

  public function sendDataTestor() {
    $indexCurrentRun = 0;
    $timeOut = FALSE;
    while (!$timeOut) {
      $timeOut = yield $indexCurrentRun;
      10 <= $timeOut && dump(sprintf('in generator %s', $timeOut));
      $indexCurrentRun++;
    }
    yield 'X of bytes are missing. </br>';
  }

}