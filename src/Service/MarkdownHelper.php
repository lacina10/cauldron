<?php

namespace App\Service;

use Knp\Bundle\MarkdownBundle\MarkdownParserInterface;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Cache\CacheInterface;

class MarkdownHelper {

  private $markdownParser;

  private $cache;

  private $isDebug;

  private $logger;

  public function __construct(
    bool $isDebug,
    MarkdownParserInterface $markdownParser,
    CacheInterface $cache,
    LoggerInterface  $mdLogger
  ) {

    $this->markdownParser = $markdownParser;
    $this->cache = $cache;
    $this->isDebug = $isDebug;
    $this->logger =  $mdLogger;
  }

  public function parse(string $source): string {
    dump($this->cache);
    if (FALSE !== stripos($source, 'cat')) {
      $this->logger->info('miouaou!');
    }
    if ($this->isDebug) {
      return $this->markdownParser->transformMarkdown($source);
    }
    return $this->cache->get('markdown_' . $source, function () use ($source) {
      return $this->markdownParser->transformMarkdown($source);
    });
  }

}